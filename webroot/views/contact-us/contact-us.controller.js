/**
 * Created by eruiz on 11/11/15.
 */
(function () {
    'use strict';

    angular.module('myApp')
        .controller('ContactUsController', ContactUsController);

    ContactUsController.$inject = ['$state', '$scope'];
    function ContactUsController($state, $scope) {
        var vm = this;

        vm.greeting = "Contact Us Page";
    }
})();
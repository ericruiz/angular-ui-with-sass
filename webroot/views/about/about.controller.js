/**
 * Created by eruiz on 11/11/15.
 */
(function () {
    'use strict';

    angular.module('myApp')
        .controller('AboutController', AboutController);

    AboutController.$inject = ['$state', '$scope'];
    function AboutController($state, $scope) {
        var vm = this;

        vm.greeting = "About Page";
    }
})();
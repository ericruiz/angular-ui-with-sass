# README #

### What is this repository for? ###

**angular-ui-with-sass project is intended to be a seed for new projects.**

It can also be used as a tutorial for learning the basics for angular and ui-router configuration  
Also included is Sass with Compass, Susy, and Breakpoint
 
* Version 1.0

### How do I get set up? ###

Additional configuration is not needed to get the project running out of the box.  

**Requirements:**  
You'll need to install Ruby (for Windows users). Then ensure Sass, Compass, and Susy gems are installed. See link below for more info. Also, GIYF =)  

Install Sass - http://sass-lang.com/install  
Install Compass - http://compass-style.org/install/  
Install Susy - http://susydocs.oddbird.net/en/latest/install/    
Install Breakpoint - https://github.com/at-import/breakpoint/wiki/installation  
  
**Dependencies included:**  
- Angular 1.4.7 (uncompressed)  
- UI-Router 0.2.15  
- 3 generic views (Html files)  
- 3 generic controllers  
- 3 generic states (see router.js)  
- Basic Sass structure  
- Generic sass files for generic views

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Eric Ruiz - ericruiz.developer@gmail.com